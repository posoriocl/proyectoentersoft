exports.port = process.argv[2] || process.env.PORT || 3000;
exports.secret = process.env.JWT_SECRET || 'entersoft'; // JWT secret
//Conexion Base de Datos PostGres
exports.portPostGres = 5432;
exports.hostPostGres = '200.111.137.118';
exports.usuarioPostGres = 'postgres';
exports.passwordPostGres = 'postgres';
exports.bdPostGres = 'entersoftdb';
//Fin Conexion Base de Datos PostGres

//Conexion Base de Datos Mongo
exports.dbMongo = process.env.DB || 'mongodb://localhost:27017/nombreBase';
// Fin Conexion Base de Datos Mongo