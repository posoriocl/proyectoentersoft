const mongose = require('mongoose')
const configure = require('./configura');
const { dbMongo } = configure;

module.exports = {
    connection: null,
    connect: async () => {
        if(this.connection) return this.connection;
        try {
            const connection = await mongose.connect(dbMongo,{ useNewUrlParser: true ,useUnifiedTopology: true,
            useNewUrlParser: true, useCreateIndex: true})
            
            console.log('conexión a base de datos Mongo Exitosa');
            return this.connection = connection;
        } catch(error){ 

            respuesta = {
                error: true, 
                data: '',
                codigo: 500, 
                mensaje: error
               };
              console.log(respuesta);
              return res.status(500).json(respuesta);
        }
    }
}