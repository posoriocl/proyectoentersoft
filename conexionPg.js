const configure = require('./configura');
const { portPostGres,hostPostGres, usuarioPostGres, passwordPostGres, bdPostGres } = configure;

const {Pool} = require('pg')
const config = {
  user: usuarioPostGres,
  host: hostPostGres,
  database: bdPostGres,
  password: passwordPostGres,
  port: portPostGres,
}

const pool = new Pool(config)
module.exports=pool;