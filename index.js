const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const configure = require('./configura');
const auth = require('./src/middlewares/authToken')
//const db = require('./conexionMongo.js'); //Conexion Mongo
const { port } = configure;
//const port = 3000


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
  })
)

app.use(auth);  // Verifica que tenga Token
//db.connect();  //conexion Mongo
// routes
//app.use(require('./src/routes'));
app.use('/api/clientes', require('./src/routes/clientes'));
app.use('/api/clientesmongo', require('./src/routes/clienteMongo'));
app.use('/api/usuariosmongo', require('./src/routes/usuarioMongo'));
app.use('/api/auth', require('./src/routes/auth'));
//app.use('/api/users', require('./routes/users'));



app.listen(port, (error) => {
    if(error) return console.log(error); 
    console.log(`Servidor corriendo en el puerto ${port}!`);
});