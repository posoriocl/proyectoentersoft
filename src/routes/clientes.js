const { Router } = require('express');
const router = new Router();
const db = require('./../../conexionPg.js');

db.connect();
router.get('/', async (req, res) => {
    try{
        await db.query('SELECT * FROM maestros.clientes ', (error, results) => {
        if (error) {
          //  throw error
          respuesta = {
            error: true, 
            data: '',
            codigo: 400, 
            mensaje: error
           };
            console.log(respuesta);
            res.status(400).json(respuesta);
        }else{
          respuesta = {
            error: false, 
            data: results.rows,
            codigo: 200, 
            mensaje: 'ok'
           };
        res.status(200).json(results.rows);
        };
        })
    } catch(e) {
      respuesta = {
        error: true, 
        data: '',
        codigo: 400, 
        mensaje: e
       };
        console.log(respuesta);
    }
  }
)

router.get('/:id', async (req, res) => {
  try{
      const  id  = parseInt(req.params.id);
      await db.query('SELECT * FROM maestros.clientes where clienteid=$1',[id], (error, results) => {
      if (error) {
        //  throw error
        respuesta = {
          error: true, 
          data: '',
          codigo: 400, 
          mensaje: error
         };
          console.log(respuesta);
          res.status(400).json(respuesta);
      }else{
        respuesta = {
          error: false, 
          data: results.rows,
          codigo: 200, 
          mensaje: 'ok'
         };
      res.status(200).json(respuesta);
      };
      })
  } catch(e) {
      respuesta = {
        error: true, 
        data: '',
        codigo: 400, 
        mensaje: e
       };
       console.log(respuesta);
  }
}
)


router.post('/', async (req, res) => {
  try{
      const { id, nombre, apellido, email, fono } = req.body
      await db.query('insert into maestros.clientes (clienteid, nombre, apellido, email, fono) VALUES ($1, $2, $3, $4, $5) ',[id, nombre, apellido, email, fono], (error, results) => {
      if (error) {
        //  throw error
        respuesta = {
          error: true, 
          data: '',
          codigo: 400, 
          mensaje: error
         };
          console.log(respuesta);
          res.status(400).json(respuesta);
      }else{
        respuesta = {
          error: false, 
          data: req.body,
          codigo: 200, 
          mensaje: 'ok'
         };
      res.status(200).json(respuesta);
      };
      })
  } catch(e) {
    respuesta = {
      error: true, 
      data: '',
      codigo: 400, 
      mensaje: 'ok'
     };
      console.log(e);
  }
}
)

router.put('/:id', async (req, res) => {
  try{
      const  id  = parseInt(req.params.id);
      const { nombre, apellido, email, fono } = req.body

      await db.query('update maestros.clientes set nombre=$1, apellido=$2, email=$3, fono=$4 where clienteid=$5 ',[nombre, apellido, email, fono, id], (error, results) => {
      if (error) {
        //  throw error
        respuesta = {
          error: true, 
          data: '',
          codigo: 400, 
          mensaje: error
         };
          console.log(respuesta);
          res.status(400).json(respuesta);
      }else{
        respuesta = {
          error: false, 
          data: req.body,
          codigo: 200, 
          mensaje: 'ok'
         };
      res.status(200).json(respuesta);
      };
      })
  } catch(e) {
    respuesta = {
      error: true, 
      data: '',
      codigo: 400, 
      mensaje: e
     };
      console.log(respuesta);
  }
}
)

router.delete('/:id', async (req, res) => {
  try{
      const  id  = parseInt(req.params.id);

      await db.query('delete from maestros.clientes where clienteid=$1 ',[id], (error, results) => {
      if (error) {
        //  throw error
        respuesta = {
          error: true, 
          data: '',
          codigo: 400, 
          mensaje: error
         };
          console.log(respuesta);
          res.status(400).json(error);
      }else{
        respuesta = {
          error: false, 
          data: results.rows,
          codigo: 200, 
          mensaje: 'ok'
         };
      res.status(200).json(respuesta);
      };
      })
  } catch(e) {
    respuesta = {
      error: true, 
      data: '',
      codigo: 400, 
      mensaje: e
     };
      console.log(respuesta);
  }
}
)
module.exports = router;