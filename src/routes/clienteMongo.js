const { Router } = require('express');
const router = new Router();
const db = require('./../../conexionMongo.js');

const clienteCrl = require('./../controlador/clienteControlador')


db.connect();
router.get('/',clienteCrl.index) // api/cliente/ Index->Listar todos los Clientes
      .post('/',clienteCrl.crear) // api/cliente/ crear -> Crear un nuevo Cliente
      .get('/:key/:valor',clienteCrl.buscaCliente,clienteCrl.mostrar) // api/cliente/idcliente Mostrar-> Muestra un cliente
      .put('/:key/:valor',clienteCrl.buscaCliente,clienteCrl.actualizar) // api/cliente/idcliente Actualiza-> Actualiza Cliente
      .delete('/:key/:valor',clienteCrl.buscaCliente,clienteCrl.eliminar) // api/cliente/idcliente

module.exports = router;