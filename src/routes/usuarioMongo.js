const { Router } = require('express');
const router = new Router();
const db = require('./../../conexionMongo.js');

const usuarioCrl = require('./../controlador/usuarioControlador')


db.connect();
router.get('/',usuarioCrl.index) // api/usuario/ Index->Listar todos los Usuarios
      .post('/',usuarioCrl.crear) // api/usuario/ crear -> Crear un nuevo Usuario
      .get('/:key/:valor',usuarioCrl.buscaUsuario,usuarioCrl.mostrar) // api/campo/valor Mostrar-> Muestra un cliente
      .put('/:key/:valor',usuarioCrl.buscaUsuario,usuarioCrl.actualizar) // api/campo/valor Actualiza-> Actualiza Cliente
      .delete('/:key/:valor',usuarioCrl.buscaUsuario,usuarioCrl.eliminar) // api/campo/valor

module.exports = router;