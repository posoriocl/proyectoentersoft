const usuario = require('./../modelo/Usuario');

async function index(req,res) {
    try {
        const usuariosRep =  await usuario.find({})
        respuesta = {
            error: false, 
            data: usuariosRep,
            codigo: 200, 
            mensaje: 'ok'
        };
        return res.status(200).json(respuesta);
    } catch(error) {
        respuesta = {
          error: true, 
          data: '',
          codigo: 500, 
          mensaje: error
        };
        console.log(respuesta);
        return res.status(500).json(respuesta);
      }   
}

function mostrar(req,res) {
    if(req.body.error){   // si la funcion de busqueda está con error
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: req.body.error
           };
            console.log(respuesta);
            return res.status(500).json(respuesta);
    }
    if(req.body.usuarios){  // si la función de busqueda encontro información
        respuesta = {
            error: false, 
            data: req.body.usuarios,
            codigo: 200, 
            mensaje: 'ok'
        };
        return res.status(200).json(respuesta);
    }
   //si no encontro registros
    respuesta = {
        error: false, 
        data: '',
        codigo: 404, 
        mensaje: 'No encontró registros'
    };
    return res.status(404).json(respuesta);



}

async function crear(req,res) {
    try {
        const usuarioRep = await new usuario(req.body).save()

        respuesta = {
            error: false, 
            data: usuario_,
            codigo: 201, 
            mensaje: 'ok'
        };
        console.log(respuesta);
        res.status(201).json(respuesta)
    } catch(error) {
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: error
        };
        console.log(respuesta);
        return res.status(500).json(respuesta);
    }   
}

async function actualizar(req,res) {

    if(req.body.error){ // Si biene un error de la busueda anterior
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: req.body.error
           };
            console.log(respuesta);
            return res.status(500).json(respuesta);
    }

    if(!req.body.usuarios){             // Si no trae información de la búsqueda anterior
        respuesta = {       
            error: true, 
            data: '',
            codigo: 404, 
            mensaje: 'No se encontro el Información'
           };
            console.log(respuesta);
            return res.status(500).json(respuesta);
    }

    // si encontro información reemplaza información
    try {
        let usuarioActualiza = req.body.usuarios[0];
 
        usuarioActualiza = Object.assign(usuarioActualiza,req.body);
        const usuarioResp= await usuarioActualiza.save();
        
        respuesta = {
            error: false, 
            data: usuarioResp,
            codigo: 200, 
            mensaje: 'ok'
        };
        console.log(respuesta);
        res.status(201).json(respuesta)
    } catch(error) {
        respuesta = {
          error: true, 
          data: '',
          codigo: 500, 
          mensaje: error
         };
          console.log(respuesta);
          return res.status(500).json(respuesta);
      }   
}

async function eliminar(req,res) {
    if(req.body.error){ // Si biene un error de la busueda anterior
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: req.body.error
           };
            console.log(respuesta);
            return res.status(500).json(respuesta);
    }

    if(!req.body.usuarios){             // Si no trae información de la búsqueda anterior
        respuesta = {       
            error: true, 
            data: '',
            codigo: 404, 
            mensaje: 'No se encontro el Información'
           };
            console.log(respuesta);
            return res.status(400).json(respuesta);
    }

    // si encontro información reemplaza información
    try {
        let usuario = req.body.usuarios[0];
        const usuarioResp = await usuario.remove()
        
        respuesta = {
            error: false, 
            data: usuarioResp,
            codigo: 200, 
            mensaje: 'ok'
        };
        console.log(respuesta);
        res.status(201).json(respuesta)
    } catch(error) {
        respuesta = {
          error: true, 
          data: '',
          codigo: 500, 
          mensaje: error
        };
        console.log(respuesta);
        res.status(500).json(respuesta);
    }   


}
//next pasa a la siguiente función
async function buscaUsuario(req,res,next){
    try {
        let query={};
        query[req.params.key]=req.params.valor;
        const usuariosRep = await usuario.find(query)
        
        if(!usuariosRep.length) return next(); // si no tiene datos pasa a la siguiente funcion
        req.body.usuarios=usuariosRep;  // si tiene datos los guarda y pasa a la siguiente funcion
        return next();
    } catch (error) {
        req.body.error = error;  // si hay un error lo guarda y pasa a la siguiente funcion
        next();
    }
}

module.exports = {
    index,
    mostrar,
    crear,
    actualizar,
    eliminar,
    buscaUsuario
}