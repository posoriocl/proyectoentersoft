const usuario = require('./../modelo/Usuario');
const configure = require('./../../configura');
const { secret } = configure;
const jwt = require('jsonwebtoken');

//Usuario
//Contraseña
async function login(req,res){
    let usuario_ = req.body.usuario;
    let contrasena = req.body.contrasena;

    const bcrypt = require('bcrypt');
    try {
        const usu = await usuario.findOne({usuario: usuario_})
        if(!usu) {
            respuesta = {
            error: false, 
            data: '',
            codigo: 404, 
            mensaje: 'No encontró Usuario'
            };
            return res.status(404).json(respuesta);
        }
                
        const match = await bcrypt.compare(contrasena,usu.contrasena)
         // match variable booleana 
        if(match)   {   //acceso
           payload = { // esto permite que el token se personalice con datos personales del usuario
                usuario: usu.usuario,
                email:  usu.email,
                nombreCompleto:  usu.nombreCompleto.nombres,
                role: usu.role
            }
            const token = await jwt.sign(payload,secret,{expiresIn: '6h'}); // token dure 24 horas --- secret es la palabra secreta para generar token
            respuesta = {
                    error: false, 
                    data: {token},
                    codigo: 200, 
                    mensaje: 'Acceso'
            };
            res.status(200).json(respuesta);
        }else{
            respuesta = {
                error: false, 
                data: usu,
                codigo: 200, 
                mensaje: 'Contraseña Incorrecta - SIN ACCESO'
            };
            res.status(200).json(respuesta);
        }      

    } catch (error) {
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: error
        };
        console.log(respuesta);
        res.status(500).json(respuesta);
    };
}

module.exports = login;