const cliente = require('./../modelo/Cliente');

async function index(req,res) {
    try {
        const clientes = await cliente.find({});
        respuesta = {
            error: false, 
            data: clientes,
            codigo: 200, 
            mensaje: 'ok'
        };
        return res.status(200).json(respuesta);
    } catch(error) {
        respuesta = {
          error: true, 
          data: '',
          codigo: 500, 
          mensaje: error
         };
        console.log(respuesta);
        return res.status(500).json(respuesta);
      }   
}

function mostrar(req,res) {
    if(req.body.error){   // si la funcion de busqueda está con error
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: req.body.error
           };
            console.log(respuesta);
            return res.status(500).json(respuesta);
    }
    if(req.body.clientes){  // si la función de busqueda encontro información
        respuesta = {
            error: false, 
            data: req.body.clientes,
            codigo: 200, 
            mensaje: 'ok'
        };
        return res.status(200).json(respuesta);
    }
   //si no encontro registros
    respuesta = {
        error: false, 
        data: '',
        codigo: 404, 
        mensaje: 'No encontró registros'
    };
    return res.status(404).json(respuesta);



}

async function crear(req,res) {
    try {
        const cliente_resp = await new cliente(req.body).save()
    
        respuesta = {
            error: false, 
            data: cliente_resp,
            codigo: 201, 
            mensaje: 'ok'
        };
        console.log(respuesta);
        res.status(201).json(respuesta)
    } catch(error) {
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: error
        };
        console.log(respuesta);
        return res.status(500).json(respuesta);
    }   
}

async function actualizar(req,res) {

    if(req.body.error){ // Si biene un error de la busueda anterior
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: req.body.error
           };
            console.log(respuesta);
            return res.status(500).json(respuesta);
    }

    if(!req.body.clientes){             // Si no trae información de la búsqueda anterior
        respuesta = {       
            error: true, 
            data: '',
            codigo: 404, 
            mensaje: 'No se encontro el Información'
           };
            console.log(respuesta);
            return res.status(404).json(respuesta);
    }

    // si encontro información reemplaza información
    try {
        let cliente_actualiza = req.body.clientes[0];
 
        cliente_actualiza = Object.assign(cliente_actualiza,req.body);
        const cliente_resp = cliente_actualiza.save()
        
        respuesta = {
            error: false, 
            data: cliente_resp,
            codigo: 200, 
            mensaje: 'ok'
        };
        console.log(respuesta);
        res.status(201).json(respuesta)
    } catch(error) {
        respuesta = {
          error: true, 
          data: '',
          codigo: 500, 
          mensaje: error
         };
          console.log(respuesta);
          return res.status(500).json(respuesta);
      }   


}

async function eliminar(req,res) {
    if(req.body.error){ // Si biene un error de la busueda anterior
        respuesta = {
            error: true, 
            data: '',
            codigo: 500, 
            mensaje: req.body.error
           };
            console.log(respuesta);
            return res.status(500).json(respuesta);
    }

    if(!req.body.clientes){             // Si no trae información de la búsqueda anterior
        respuesta = {       
            error: true, 
            data: '',
            codigo: 404, 
            mensaje: 'No se encontro el Información'
           };
            console.log(respuesta);
            return res.status(400).json(respuesta);
    }

    // si encontro información reemplaza información
    try {
        let cliente = req.body.clientes[0];
        const cliente_resp = await cliente.remove()
        
        respuesta = {
            error: false, 
            data: cliente_resp,
            codigo: 200, 
            mensaje: 'ok'
        };
        console.log(respuesta);
        res.status(201).json(respuesta)
    } catch(error){
        respuesta = {
          error: true, 
          data: '',
          codigo: 500, 
          mensaje: error
         };
          console.log(respuesta);
          return res.status(500).json(respuesta);
      }   


}
//next pasa a la siguiente función
async function buscaCliente(req,res,next){
    console.log('paso');
    try {
        let query={};
        query[req.params.key]=req.params.valor;
        console.log(query);
        const clientes = await cliente.find(query)
    
        if(!clientes.length) return next(); // si no tiene datos pasa a la siguiente funcion
        req.body.clientes=clientes;  // si tiene datos los guarda y pasa a la siguiente funcion
        return next();
    } catch(error) {
        req.body.error = error;  // si hay un error lo guarda y pasa a la siguiente funcion
        next();
    }
}

module.exports = {
    index,
    mostrar,
    crear,
    actualizar,
    eliminar,
    buscaCliente
}