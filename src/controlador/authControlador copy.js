const usuario = require('./../modelo/Usuario');
const configure = require('./../../configura');
const { secret } = configure;
const jwt = require('jsonwebtoken');

//Usuario
//Contraseña
function login(req,res){
    let usuario_ = req.body.usuario;
    let contrasena = req.body.contrasena;

    const bcrypt = require('bcrypt');

    usuario.findOne({usuario: usuario_})
           .then(usu => {
                if(!usu) {
                    respuesta = {
                        error: false, 
                        data: '',
                        codigo: 404, 
                        mensaje: 'No encontró Usuario'
                    };
                    return res.status(404).json(respuesta);
                }
                
                bcrypt.compare(contrasena,usu.contrasena)
                      .then(match =>{ // match variable booleana 
                            if(match)   {   //acceso
                                payload = { // esto permite que el token se personalice con datos personales del usuario
                                    usuario: usu.usuario,
                                    email:  usu.email,
                                    nombreCompleto:  usu.nombreCompleto.nombres,
                                    role: usu.role
                                }
                                jwt.sign(payload,secret,function(error,token){ // secret es la palabra secreta para generar token
                                    if(error){
                                        respuesta = {
                                            error: true, 
                                            data: '',
                                            codigo: 500, 
                                            mensaje: error
                                           };
                                            console.log(respuesta);
                                            res.status(500).json(respuesta);
                                    }else{
                                        respuesta = {
                                            error: false, 
                                            data: {token},
                                            codigo: 200, 
                                            mensaje: 'Acceso'
                                        };
                                        res.status(200).json(respuesta);
                                    }
                                })   
                            }else{
                                respuesta = {
                                    error: false, 
                                    data: usu,
                                    codigo: 200, 
                                    mensaje: 'Contraseña Incorrecta - SIN ACCESO'
                                };
                                res.status(200).json(respuesta);
                            }
                      }).catch(error => {
                        respuesta = {
                            error: true, 
                            data: '',
                            codigo: 500, 
                            mensaje: error
                           };
                            console.log(respuesta);
                            res.status(500).json(respuesta);
                      });

           }).catch(error => {
            respuesta = {
                error: true, 
                data: '',
                codigo: 500, 
                mensaje: error
               };
                console.log(respuesta);
                res.status(500).json(respuesta);
           });
}

module.exports = login;