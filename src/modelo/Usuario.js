const mongoose = require('mongoose');
const bcrypt = require('bcrypt'); // permite encriptar

const UsuarioSchema = new mongoose.Schema({
    usuario:{
        type:       String,
        unique:     true,
        required:   true
    },
    contrasena:{
        type:       String,
        unique:     true,
        required:   true
    },
    nombreCompleto:{
        nombres:{   type:       String,
                    required:   true
        },
        apellidoPaterno:{   type:       String,
                            required:   true
        },
        apellidoMaterno:{   type:       String,
                            required:   true
        }
    },
    email:{
        type:       String,
        unique:     true,
        required:   true
    },
    fono:{
        type:       Number,
        required:   true,
        default:    0
    },
    role: {
        type: String,
        default:'regular',
        enum:[
            'regular',
            'admin'
        ]
    },
    fechaRegistro:{
        type:       Date,
        default:    Date.now()
    },
    fechaUltimoAcceso:{
        type:       Date,
        default:    Date.now()
    }
});

//realiza encriptación antes que se genere el metodo SAVE
UsuarioSchema.pre('save',function(next){
    bcrypt.genSalt(10).then(salts =>{
        bcrypt.hash(this.contrasena,salts).then(hash => {
            this.contrasena = hash;
            next();
        }).catch(error => next(error));
    }).catch(error => next(error));
});
// Fin Encriptación

const Usuario = mongoose.model('Usuario',UsuarioSchema);

module.exports = Usuario;