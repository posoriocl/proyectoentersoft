const mongoose = require('mongoose');

const ClienteSchema = new mongoose.Schema({
    nombreCompleto:{
        nombres:{   type:       String,
                    required:   true
        },
        apellidoPaterno:{   type:       String,
                            required:   true
        },
        apellidoMaterno:{   type:       String,
                            required:   true
        }
    },
    email:{
        type:       String,
        unique:     true,
        required:   true
    },
    fono:{
        type:       Number,
        required:   true,
        default:    0
    }
});


const Cliente = mongoose.model('Cliente',ClienteSchema);

module.exports = Cliente;