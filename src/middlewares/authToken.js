const jwt =require('jsonwebtoken');
const configure = require('./../../configura');
const { secret } = configure;

module.exports = function (req,res,next) {
    if(req.path != '/api/auth/login'){  // verifica la ruta, solo debe entrar la ruta de login
        if(req.headers.authorization){  //verifica que tenga cabecera
            ultimo=req.headers.authorization.split(' ').length-1; // verifica tomar el último registro si es que tiene espacios el token
            let token = req.headers.authorization.split(' ')[ultimo];  //Rescata token del headers
            jwt.verify(token,secret,function (error,decoded){  //compara el token actual con el entregado
                if(error){
                    respuesta = {
                        error: true, 
                        data: '',
                        codigo: 403, 
                        mensaje: 'No tiene Autorización2'
                       };
                        console.log(respuesta);
                        return res.status(403).json(respuesta);
                }
                if(req.method != 'GET'){
                    console.log('rol:', decoded.role)
                    if(decoded.role == 'admin'){
                        next();
                    }else{
                        respuesta = {
                            error: true, 
                            data: '',
                            codigo: 403, 
                            mensaje: "No tiene Permiso"
                        };
                        console.log(respuesta);
                        res.status(403).json(respuesta);
                    }
                }else{
                    next();
                }
            })

        }else{
            respuesta = {
                error: true, 
                data: '',
                codigo: 403, 
                mensaje: "No tiene Autorización"
               };
                console.log(respuesta);
                res.status(403).json(respuesta);
        }
    }else next();
}